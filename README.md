# RMM ReactJS Boilerplate

Boilerplate pessoal para criação de aplicações, pocs e estudo da tecnologia.

- ReactJs 17.0.x
- Iniciado com [create-react-app](https://create-react-app.dev/docs/getting-started/)

## Requisitos

- Node > 14.x
- Npm > 6.x

## Instalação

Para clonar o projeto

```bash
$ git clone git@gitlab.com:ricardo-melo-martins/rmm-reactjs-boilerplate.git
```

Instale os pacotes com o comando

```bash
$ npm install
```

Para executar o projeto

```bash
$ npm start
```

Abra o navegador e digite a url
http://127.0.0.1:3000

## Referência

[ReactJS Documentação Oficial](https://reactjs.org/docs/getting-started.html)

[Create-react-app Documentação Oficial](https://create-react-app.dev/docs/getting-started/)

## Licença

O RMM ReactJS Boilerplate é de código aberto licenciado sob a [licença MIT](https://opensource.org/licenses/MIT).
